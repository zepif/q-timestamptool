package ingest

import (
	"context"
	"time"

	"gitlab.com/q-dev/q-client/ethclient"
)

type SearchResult struct {
	BlockNumber uint64
	Operations  int
	Duration    time.Duration
}

type Service struct {
	client *ethclient.Client
}

func NewService(client *ethclient.Client) *Service {
	return &Service{
		client: client,
	}
}

func (s *Service) GetBlockNumberAtTimestampLinear(ctx context.Context, timestamp int64) (SearchResult, error) {
	start := time.Now()
	blockNumber, operations, err := LinearSearchBlockByTimestamp(ctx, s.client, timestamp)
	duration := time.Since(start)
	return SearchResult{BlockNumber: blockNumber, Operations: operations, Duration: duration}, err
}

func (s *Service) GetBlockNumberAtTimestampBinary(ctx context.Context, timestamp int64) (SearchResult, error) {
	start := time.Now()
	blockNumber, operations, err := BinarySearchBlockByTimestamp(ctx, s.client, timestamp)
	duration := time.Since(start)
	return SearchResult{BlockNumber: blockNumber, Operations: operations, Duration: duration}, err
}

func (s *Service) GetBlockNumberAtTimestampInterpolation(ctx context.Context, timestamp int64) (SearchResult, error) {
	start := time.Now()
	blockNumber, operations, err := InterpolationSearchBlockByTimestamp(ctx, s.client, timestamp)
	duration := time.Since(start)
	return SearchResult{BlockNumber: blockNumber, Operations: operations, Duration: duration}, err
}

func (s *Service) GetBlockNumberAtTimestampOptimizedBinary(ctx context.Context, timestamp int64) (SearchResult, error) {
	start := time.Now()
	blockNumber, operations, err := OptimizedBinarySearchBlockByTimestamp(ctx, s.client, timestamp)
	duration := time.Since(start)
	return SearchResult{BlockNumber: blockNumber, Operations: operations, Duration: duration}, err
}
