package ingest

import (
	"context"
	"math/big"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/ethclient"
)

func LinearSearchBlockByTimestamp(ctx context.Context, client *ethclient.Client, targetTimestamp int64) (uint64, int, error) {
	latestBlock, err := client.HeaderByNumber(ctx, nil)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to get latest block")
	}

	operations := 1
	for blockNum := latestBlock.Number.Uint64(); blockNum > 0; blockNum-- {
		select {
		case <-ctx.Done():
			return 0, operations, ctx.Err()
		default:
			block, err := client.HeaderByNumber(ctx, big.NewInt(int64(blockNum)))
			operations++
			if err != nil {
				return 0, operations, errors.Wrap(err, "failed to get block")
			}
			if int64(block.Time) <= targetTimestamp {
				return blockNum, operations, nil
			}
		}
	}
	return 0, operations, errors.New("block not found")
}
