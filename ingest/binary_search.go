package ingest

import (
	"context"
	"math/big"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/ethclient"
)

func BinarySearchBlockByTimestamp(ctx context.Context, client *ethclient.Client, targetTimestamp int64) (uint64, int, error) {
	latestBlock, err := client.HeaderByNumber(ctx, nil)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to get latest block")
	}

	latestBlockTime := uint64(latestBlock.Time)
	avgBlockTime := latestBlockTime / (latestBlock.Number.Uint64() - 1)
	operations := 1
	low, high := uint64(targetTimestamp/(int64(avgBlockTime)+1)), latestBlock.Number.Uint64()
	// fmt.Printf("%d %d", low, high)
	for low <= high {
		select {
		case <-ctx.Done():
			return 0, operations, ctx.Err()
		default:
			mid := (low + high) / 2
			block, err := client.HeaderByNumber(ctx, big.NewInt(int64(mid)))
			operations++
			if err != nil {
				return 0, operations, errors.Wrap(err, "failed to get block")
			}
			blockTime := int64(block.Time)
			if blockTime == targetTimestamp {
				return uint64(mid), operations, nil
			} else if blockTime < targetTimestamp {
				low = mid + 1
			} else {
				high = mid - 1
			}
		}
	}
	return uint64(high), operations, nil
}
