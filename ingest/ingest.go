package ingest

import (
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/zepif/q-timestamptool/config"
)

func New(cfg config.Config) (*Service, error) {
	client := cfg.Client()
	if client == nil {
		return nil, errors.New("client is not initialized")
	}

	return NewService(client), nil
}
