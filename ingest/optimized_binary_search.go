package ingest

import (
	"context"
	"math/big"
	"sync"
	"time"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/core/types"
	"gitlab.com/q-dev/q-client/ethclient"
)

const (
	maxRetries    = 3
	retryInterval = 500 * time.Millisecond
	batchSize     = 4
)

var blockCache sync.Map

func OptimizedBinarySearchBlockByTimestamp(ctx context.Context, client *ethclient.Client, targetTimestamp int64) (uint64, int, error) {
	latestBlock, _, err := getBlockWithRetry(ctx, client, nil)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to get latest block")
	}

	operations := 1
	latestBlockTime := uint64(latestBlock.Time)
	avgBlockTime := latestBlockTime / (latestBlock.Number.Uint64() - 1)
	low, high := uint64(targetTimestamp/(int64(avgBlockTime)+1)), latestBlock.Number.Uint64()

	for low < high {
		select {
		case <-ctx.Done():
			return 0, operations, ctx.Err()
		default:
			mid := (high + low) / 2
			blocks, cached, err := getBatchBlocks(ctx, client, mid)
			operations += len(blocks) - int(cached)
			if err != nil {
				return 0, operations, errors.Wrap(err, "failed to get batch blocks")
			}

			for i, block := range blocks {
				blockTime := int64(block.Time)
				if blockTime == targetTimestamp {
					return mid + uint64(i), operations, nil
				}
				if blockTime < targetTimestamp {
					low = mid + uint64(i) + 1
				} else {
					high = mid + uint64(i)
					break
				}
			}
		}
	}

	return high - 1, operations, nil
}

func getBlockWithRetry(ctx context.Context, client *ethclient.Client, number *big.Int) (*types.Header, bool, error) {
	var lastErr error
	for i := 0; i < maxRetries; i++ {
		block, isCached, err := getCachedBlock(ctx, client, number)
		if err == nil {
			return block, isCached, nil
		}
		lastErr = err
		time.Sleep(retryInterval)
	}
	return nil, false, lastErr
}

func getCachedBlock(ctx context.Context, client *ethclient.Client, number *big.Int) (*types.Header, bool, error) {
	var key interface{}
	if number == nil {
		key = "latest"
	} else {
		key = number.Uint64()
	}

	if cachedBlock, ok := blockCache.Load(key); ok {
		return cachedBlock.(*types.Header), true, nil
	}

	block, err := client.HeaderByNumber(ctx, number)
	if err != nil {
		return nil, false, err
	}

	blockCache.Store(key, block)
	return block, false, nil
}

func getBatchBlocks(ctx context.Context, client *ethclient.Client, startBlock uint64) ([]*types.Header, int64, error) {
	var wg sync.WaitGroup
	blocks := make([]*types.Header, batchSize)
	errors := make([]error, batchSize)
	cached := make([]bool, batchSize)

	for i := 0; i < batchSize; i++ {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			block, isCached, err := getBlockWithRetry(ctx, client, big.NewInt(int64(startBlock+uint64(index))))
			if err != nil {
				errors[index] = err
				return
			}
			blocks[index] = block
			cached[index] = isCached
		}(i)
	}

	wg.Wait()
	for _, err := range errors {
		if err != nil {
			return nil, 0, err
		}
	}
	cachedCount := int64(0)
	for _, isCached := range cached {
		if isCached {
			cachedCount++
		}
	}
	return blocks, cachedCount, nil
}
