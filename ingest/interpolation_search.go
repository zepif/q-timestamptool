package ingest

import (
	"context"
	"math/big"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/ethclient"
)

func InterpolationSearchBlockByTimestamp(ctx context.Context, client *ethclient.Client, targetTimestamp int64) (uint64, int, error) {
	latestBlock, err := client.HeaderByNumber(ctx, nil)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to get latest block")
	}

	operations := 1
	low, high := uint64(0), latestBlock.Number.Uint64()
	lowTime, highTime := int64(0), int64(latestBlock.Time)
	for low <= high && targetTimestamp >= lowTime && targetTimestamp <= highTime {
		select {
		case <-ctx.Done():
			return 0, operations, ctx.Err()
		default:
			if highTime == lowTime {
				if int64(latestBlock.Time) == targetTimestamp {
					return high, operations, nil
				}
				return 0, operations, errors.New("block not found")
			}
			pos := low + uint64(float64(high-low)*float64(targetTimestamp-lowTime)/float64(highTime-lowTime))
			block, err := client.HeaderByNumber(ctx, big.NewInt(int64(pos)))
			operations++
			if err != nil {
				return 0, operations, errors.Wrap(err, "failed to get block")
			}
			blockTime := int64(block.Time)
			if blockTime == targetTimestamp {
				return pos, operations, nil
			}
			if blockTime < targetTimestamp {
				low = pos + 1
				lowTime = blockTime
			} else {
				high = pos - 1
				highTime = blockTime
			}
		}
	}
	return high, operations, nil
}
