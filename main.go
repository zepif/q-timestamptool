package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/zepif/q-timestamptool/config"
	"gitlab.com/zepif/q-timestamptool/ingest"
)

func main() {
	cfg := config.New(kv.MustFromEnv())
	log := cfg.Log()
	ingestService, err := ingest.New(cfg)
	if err != nil {
		log.WithError(err).Error("failed to create ingest service")
		os.Exit(1)
	}

	// Number of block that was an hour ago
	timestamp := time.Now().Add(-1 * time.Hour).Unix()
	ctx, cancel := context.WithTimeout(context.Background(), 120*time.Second)
	defer cancel()

	/*resultLinear, err := ingestService.GetBlockNumberAtTimestampLinear(ctx, timestamp)
	if err != nil {
		log.WithError(err).Error("failed to get block number using linear search")
	}
	fmt.Printf("Block number at timestamp %d (linear search): %d\n", timestamp, resultLinear.BlockNumber)
	fmt.Printf("Linear search operations: %d\n", resultLinear.Operations)
	fmt.Printf("Linear search duration: %s\n\n", resultLinear.Duration)*/

	resultInterpolation, err := ingestService.GetBlockNumberAtTimestampInterpolation(ctx, timestamp)
	if err != nil {
		log.WithError(err).Error("failed to get block number using interpolation search")
	}
	fmt.Printf("Block number at timestamp %d (interpolation search): %d\n", timestamp, resultInterpolation.BlockNumber)
	fmt.Printf("Interpolation search operations: %d\n", resultInterpolation.Operations)
	fmt.Printf("Interpolation search duration: %s\n\n", resultInterpolation.Duration)

	resultBinary, err := ingestService.GetBlockNumberAtTimestampBinary(ctx, timestamp)
	if err != nil {
		log.WithError(err).Error("failed to get block number using binary search")
	}
	fmt.Printf("Block number at timestamp %d (binary search): %d\n", timestamp, resultBinary.BlockNumber)
	fmt.Printf("Binary search operations: %d\n", resultBinary.Operations)
	fmt.Printf("Binary search duration: %s\n\n", resultBinary.Duration)

	resultOptimizedBinary, err := ingestService.GetBlockNumberAtTimestampOptimizedBinary(ctx, timestamp)
	if err != nil {
		log.WithError(err).Error("failed to get block number using optimized binary search")
	}
	fmt.Printf("Block number at timestamp %d (optimized binary search): %d\n", timestamp, resultOptimizedBinary.BlockNumber)
	fmt.Printf("Optimized binary search operations: %d\n", resultOptimizedBinary.Operations)
	fmt.Printf("Optimized binary search duration: %s\n", resultOptimizedBinary.Duration)
}
