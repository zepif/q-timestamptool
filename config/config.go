package config

import (
	"net/url"
	"reflect"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/ethclient"
)

type Config interface {
	comfig.Logger
	Client() *ethclient.Client
	ContractsRegistry() ContractsRegistry
	Listener() Listener
}

func New(getter kv.Getter) Config {
	return &config{
		getter: getter,
		Logger: comfig.NewLogger(getter, comfig.LoggerOpts{}),
	}
}

type config struct {
	getter kv.Getter
	comfig.Logger
	qclient           comfig.Once
	contractsRegistry comfig.Once
	listener          comfig.Once
}

type qclient struct {
	RPC *url.URL `fig:"http.url,required"`
	WS  *url.URL `fig:"ws.url"`
}

type Listener struct {
	Addr string `fig:"addr,required"`
}

type ContractsRegistry struct {
	Registry       *common.Address `fig:"registry"`
	RewardReceiver *common.Address `fig:"reward_receiver"`
}

func (c *config) Client() *ethclient.Client {
	return c.qclient.Do(func() interface{} {
		var cfg qclient
		err := figure.Out(&cfg).From(kv.MustGetStringMap(c.getter, "qclient")).Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to load qclient config"))
		}

		client, err := ethclient.Dial(cfg.WS.String())
		if err != nil {
			panic(errors.Wrap(err, "failed to init client of q node"))
		}

		return client
	}).(*ethclient.Client)
}

func (c *config) ContractsRegistry() ContractsRegistry {
	return c.contractsRegistry.Do(func() interface{} {
		var cfg ContractsRegistry
		err := figure.
			Out(&cfg).
			From(kv.MustGetStringMap(c.getter, "contracts_registry")).
			With(figure.Hooks{
				"*common.Address": func(value interface{}) (reflect.Value, error) {
					switch v := value.(type) {
					case string:
						if !common.IsHexAddress(v) {
							return reflect.Value{}, errors.New("invalid address")
						}
						addr := common.HexToAddress(v)
						return reflect.ValueOf(&addr), nil
					default:
						return reflect.Value{}, errors.Errorf("unsupported conversion from %T", value)
					}
				},
			}).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to load contracts registry config"))
		}

		return cfg
	}).(ContractsRegistry)
}

func (c *config) Listener() Listener {
	return c.listener.Do(func() interface{} {
		var cfg Listener
		err := figure.Out(&cfg).From(kv.MustGetStringMap(c.getter, "listener")).Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to load listener config"))
		}
		return cfg
	}).(Listener)
}
